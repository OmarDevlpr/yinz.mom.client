// libs
import * as assert from "assert";
import { Chance } from "chance";
import * as _ from "lodash";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import RedisMomClient from "../../main/ts/RedisMomClient";

// test dependencies
import * as IORedis from "ioredis";
import { Logger } from "@yinz/commons";


const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const logger = container.getBean<Logger>('logger');
logger;

const redisMomClient = container.getBean<RedisMomClient>("redisMomClient");

let redis;

let stopEchoing = false;

describe('| mom.client <Submit> ', function () {

    let urns = new Array(chance.natural({ min: 10, max: 20 }));
    for (let index = 0; index < urns.length; index++) {
        urns[index] = chance.string({ length: 10 }) + ":" + chance.string({ length: 10 });
    }

    before(() => {

        let echo = async (options) => {
            options = options || {};

            if (!options.logger) {
                options.logger = logger;                
            }

            
            try {
                options.logger.info('waiting messages on [%s] for [1s]...', JSON.stringify(urns));
                let packet = await redis.blpop(urns, 1);
            
            
                if (packet !== null) {
                    let msg = JSON.parse(packet[1]);
                    options.logger.info('pushing on [%s] message [%s]...', msg.replyTo, JSON.stringify(msg));                    
                    return redis.rpush(msg.replyTo, JSON.stringify(msg));
                }
                
                options.logger.info('... no message received on [%s] during the last [1s]...', JSON.stringify(urns));

            } catch (e) {
                options.logger.warn('echo: %s', e.stack || e.message);  
            }
            finally {
                if (!stopEchoing) {
                    echo(options);
                }
            }
                
        }

                    
        let redisOpts = {
            host: '192.168.99.100',
            port: 6379,
            lazyConnect: true
        }
        

        return redisMomClient.open({timeout: 2})
        .then(() => redis = new IORedis(redisOpts))
        .then( () => {
            echo({})
        })

      
    });

    after(() => {
        return new Promise(async (resolve) => {            
            stopEchoing = true
                                                
            await redisMomClient.close();            
            redis.disconnect()                       
            resolve(true);
        })
    });

    it('| submit --> should submit string message and get result successfully', async () => {

        // 1. prepare test data

        let options = { timeout: 2 };

        let message = chance.string({ length: 10 });

        let urn = chance.pick(urns);

        // execute test
                        
        let result = await redisMomClient.submit(urn, message, options);

        // 3. compare result        
        assert.strictEqual(result, message);                                
        console.log('we here ?s')
    });

    it('| submit --> should submit a int message and get result successfully', async () => {

        // 1. prepare test data

        let options = { timeout: 2 };

        let message = chance.integer();

        let urn = chance.pick(urns);

        // execute test

        let result = await redisMomClient.submit(urn, message, options);

        // 3. compare result        
        assert.strictEqual(result, message);

    });


    it('| submit --> should submit a float message and get result successfully', async () => {

        // 1. prepare test data

        let options = { timeout: 2 };

        let message = chance.floating();

        let urn = chance.pick(urns);

        // execute test

        let result = await redisMomClient.submit(urn, message, options);

        // 3. compare result        
        assert.strictEqual(result, message);

    });

    it('| submit --> should submit a date message and get result successfully', async () => {

        // 1. prepare test data

        let options = { timeout: 2 };

        let message = chance.date();

        let urn = chance.pick(urns);

        // execute test

        let result = await redisMomClient.submit(urn, message, options);

        // 3. compare result        
        assert.strictEqual(result, message.toISOString());

    });

    it('| submit --> should submit an object message and get result successfully', async () => {

        // 1. prepare test data

        let options = { timeout: 2 };

        let message = {
            id: chance.natural(),
            str: chance.string({ length: 10 }),
            date: chance.date(),
            email: chance.email(),
            phone: chance.phone(),
        };

        let urn = chance.pick(urns);

        // execute test

        let result = await redisMomClient.submit(urn, message, options);

        message.date = message.date.toISOString() as any;
        // 3. compare result        
        assert.deepStrictEqual(result, message);

    });

    

    it('| submit --> should submit many string messages and to the same urn get result successfully', async () => {

        // 1. prepare test data


        let messages = new Array(chance.natural({ min: 500, max: 1000 }));
        for (let index = 0; index < messages.length; index++) {
            messages[index] = chance.string({ length: 10 });
        }

        let options = {timeout: 2};
        
        let urn = chance.pick(urns);

        let results: any = [];

        // execute test

        for ( let message of messages ) {
            let result = await redisMomClient.submit(urn, message, options);
            results.push(result);
        }
                        
        // 3. compare result        
        assert.deepStrictEqual(results, messages);

    });

});
