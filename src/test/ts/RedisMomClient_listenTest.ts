// libs
import * as assert from "assert";
import { Chance } from "chance";
import * as _ from "lodash";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import RedisMomClient from "../../main/ts/RedisMomClient";

// test dependencies
import * as IORedis from "ioredis";
import MsgLoggerMsgFilter from "main/ts/MsgLoggerMsgFilter";


const chance = Chance();
const container = new Container({
    params: {srvmng: {appName: "test"}},
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',        
        process.cwd() + '/dist/main/ts'
    ],
});


const logger = container.getBean<Logger>('logger');

const redisMomClient = container.getBean<RedisMomClient>("redisMomClient");

let redis;

describe('| mom.client <Listen> ', function () {

    before(() => {
        return new Promise(async (resolve) => {
            await redisMomClient.open();
            let msgFilter = container.getBean<MsgLoggerMsgFilter>("msgLoggerMsgFilter");
            redisMomClient.subscribe(msgFilter);
            let redisOpts = {
                host: '192.168.99.100',
                port: 6379,
                lazyConnect: true
            }
            redis = new IORedis(redisOpts);
            resolve(true);
        })
    });
    
    after(() => {
        return new Promise(async (resolve) => {
            await redisMomClient.close();
            redis.disconnect()
            resolve(true);
        })
    });

    
    it('| listen --> should receive a string message successfully', async () => {
        
        // 1. prepare test data

        let options = {};

        let urn     = chance.string({ length: 10 });
        let message = chance.string({ length: 10 });        
                        
        // listener id
        let lid: any =  "momcliTest#listener:" + urn;

        logger.info('pushing on [%s] message [%s]...', urn, message);
        await redis.rpush(urn, JSON.stringify(message));

        // 2. execute test
                    
        
        await new Promise( async (resolve, reject) => {

            return redisMomClient.listen([urn], (urn, result, options) => {
                
                try {
                    // 3. compare test data                    
                    assert.strictEqual(result, message);                       
                    resolve();
                } catch (e) { 
                    reject(e);
                }                
            }, lid , options)     

        })
        .then( async ____ => {            
            
            await redisMomClient.cancelListener(lid, true, options);
            
        }).catch( async e => {                        
            
            await redisMomClient.cancelListener(lid, false, options);                            
            assert.fail(e)
        })                                  

    });

    it('| listen --> should receive an int message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let message = chance.integer();
        

        // listener id
        let lid: any = "momcliTest#listener:" + urn;

        logger.info('pushing on [%s] message [%s]...', urn, message);
        await redis.rpush(urn, JSON.stringify(message));

        // 2. execute test


        await new Promise(async (resolve, reject) => {

            return redisMomClient.listen([urn], (urn, result, options) => {

                try {
                    // 3. compare test data
                    assert.strictEqual(result, message);
                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, lid, options)

        })
        .then(async ____ => {

            await redisMomClient.cancelListener(lid, true, options);

        }).catch(async e => {

            await redisMomClient.cancelListener(lid, false, options);
            assert.fail(e)
        })     

    });


    it('| listen --> should receive an float message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let message = chance.floating();

        // listener id
        let lid: any = "momcliTest#listener:" + urn;

        logger.info('pushing on [%s] message [%s]...', urn, message);
        await redis.rpush(urn, JSON.stringify(message));

        // 2. execute test


        await new Promise(async (resolve, reject) => {

            return redisMomClient.listen([urn], (urn, result, options) => {

                try {
                    // 3. compare test data
                    assert.strictEqual(result, message);
                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, lid, options)

        })
        .then(async ____ => {

            await redisMomClient.cancelListener(lid, true, options);

        }).catch(async e => {

            await redisMomClient.cancelListener(lid, false, options);
            assert.fail(e)
        })     

    });



    it('| listen --> should receive a date message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let message = chance.date();

        // listener id
        let lid: any = "momcliTest#listener:" + urn;

        logger.info('pushing on [%s] message [%s]...', urn, message);
        await redis.rpush(urn, JSON.stringify(message));

        // 2. execute test


        await new Promise(async (resolve, reject) => {

            return redisMomClient.listen([urn], (urn, result, options) => {

                try {
                    // 3. compare test data
                    message = message.toISOString() as any;
                    assert.strictEqual(result, message);
                    resolve();
                } catch (e) {
                    reject(e);
                }
            }, lid, options)

        })
        .then(async ____ => {

            await redisMomClient.cancelListener(lid, true, options);

        }).catch(async e => {

            await redisMomClient.cancelListener(lid, false, options);
            assert.fail(e)
        })     

    });



    it('| listen --> should receive an object message successfully', async () => {

        // 1. prepare test data
                
        let options = {};

        let urn = chance.string({ length: 10 });
        let message = {
            id: chance.natural(),
            str: chance.string({ length: 10 }),
            date: chance.date(),
            email: chance.email(),
            phone: chance.phone(),
        };

        // listener id
        let lid: any = "momcliTest#listener:" + urn;

        logger.info('pushing on [%s] message [%s]...', urn, message);
        await redis.rpush(urn, JSON.stringify(message));

        // 2. execute test


        await new Promise(async (resolve, reject) => {

            return redisMomClient.listen([urn], (urn, result, options) => {

                try {
                    message.date = message.date.toISOString() as any;
                    delete result.seq;
                    // 3. compare test data                    
                    assert.deepStrictEqual(result, message);
                    resolve();
                } catch (e) {                    
                    reject(e);
                }
            }, lid, options)

        })
        .then(async ____ => {

            await redisMomClient.cancelListener(lid, true, options);

        }).catch(async e => {

            await redisMomClient.cancelListener(lid, false, options);
            assert.fail(e)
        })     

    });


    it('| listen --> should receive multiple string messages on the same urn successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });      
                        
        // listener id
        let lid: any = "momcliTest#listener:" + urn;
        

        let messages = new Array<string>(chance.natural({ min: 500, max: 1000 }))
        
        let result: any[] = []

        for (let index = 0; index < messages.length; index ++) {

            messages[index] = chance.string({length: 10});            

            await redis.rpush(urn, JSON.stringify(messages[index]));

        }
            
        this.title = this.title + '\n| * ' + messages.length + ' messages have been posted on the same urn\n';        

        // 2. execute test


        await new Promise(async (resolve, reject) => {

            return redisMomClient.listen([urn], (urn, msg, options) => {

                try {
                    result.push(msg)
                    // 3. compare test data                    
                    if (result.length >= messages.length) {
                        logger.info('All [%d] have been received! will shutdown the client.', result.length);
                        resolve();
                    }
                } catch (e) {
                    reject(e);
                }
            }, lid, options)

        })
        .then(async ____ => {            
            assert.deepStrictEqual(result, messages);
            await redisMomClient.cancelListener(lid, true, options);

        }).catch(async e => {

            await redisMomClient.cancelListener(lid, false, options);
            assert.fail(e)
        })     
        

    });


    it('| listen --> should receive multiple object messages on the same urn successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });

        // listener id
        let lid: any = "momcliTest#listener:" + urn;


        let messages = new Array<any>(chance.natural({ min: 500, max: 1000 }))

        let result: any[] = []


        for (let index = 0; index < messages.length; index++) {

            messages[index] =  {
                id            : chance.natural(),
                str           : chance.string({length: 10}),                
                email         : chance.email(),
                phone         : chance.phone(),
            };

            await redis.rpush(urn, JSON.stringify(messages[index]));

        }
      
        this.title = this.title + '\n| * ' + messages.length + ' messages have been posted on the same urn\n';

        // 2. execute test


        await new Promise(async (resolve, reject) => {

            return redisMomClient.listen([urn], (urn, msg, options) => {

                try {
                    
                    delete msg.seq;

                    result.push(msg)
                    // 3. compare test data                    
                    if (result.length >= messages.length) {
                        logger.info('All [%d] have been received! will shutdown the client.', result.length);
                        resolve();
                    }
                } catch (e) {
                    reject(e);
                }
            }, lid, options)

        })
        .then(async ____ => {
            assert.deepStrictEqual(result, messages);
            await redisMomClient.cancelListener(lid, true, options);

        }).catch(async e => {

            await redisMomClient.cancelListener(lid, false, options);
            assert.fail(e)
        })     

    });


    it('| listen --> should receive multiple string messages on multiple urns successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urns = new Array(chance.natural({min: 10, max: 20}));

        for (let index = 0; index < urns.length; index++ ) {
            urns[index] = chance.string({ length: 10 }) + ":" + _.padStart('' + index, 9, '0');
        }   
                       
        // listener id
        let lid: any = "momcliTest#listener:" + chance.string({length: 10});


        let messages = new Array(chance.natural({ min: 500, max: 1000 }));

        let result: any[] = []

        for ( let index = 0; index < messages.length; index++ ) {

            let value = messages[index] = 'M' + _.padStart('' + index, 9, '0');

            let key = urns[index % urns.length];
            
            logger.info('pushing on [%s] message [%s]...', key, value);            

            await redis.rpush(key, JSON.stringify(value));

        }

        this.title = this.title + '\n | * ' + messages.length + ' messages have been posted on ' + urns.length + ' urns \n';        

        // 2. execute test
        
        await new Promise(async (resolve, reject) => {

            return redisMomClient.listen(urns, (urn, msg, options) => {

                try {                    
                    result.push(msg)
                    // 3. compare test data                    
                    if (result.length === messages.length) {
                        logger.info('All [%d] have been received! will shutdown the client.', result.length);
                        resolve();
                    }
                } catch (e) {
                    reject(e);
                }
            }, lid, options)

        })
        .then(async ____ => {
            assert.strictEqual(result.length, messages.length);

            for (let index = 0; index < messages.length; index++) {
                if (result.indexOf(messages[index]) === -1) {
                    throw (`message [${messages[index]}] was not received!`);
                }
            }

            await redisMomClient.cancelListener(lid, true, options);

        }).catch(async e => {

            await redisMomClient.cancelListener(lid, false, options);
            assert.fail(e)
        })     
        
    });


});
