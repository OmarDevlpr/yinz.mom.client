// libs
import * as assert from "assert";
import { Chance } from "chance";
import * as _ from "lodash";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
// import { Logger } from "@yinz/commons";
import RedisMomClient from "../../main/ts/RedisMomClient";

// test dependencies
import * as IORedis from "ioredis";


const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        process.cwd() + '/dist/main/ts'
    ],
});


// const logger = container.getBean<Logger>('logger');

const redisMomClient = container.getBean<RedisMomClient>("redisMomClient");

let redis;

describe('| mom.client <Post> ', function () {

    before(() => {
        return new Promise(async (resolve) => {
            let redisOpts = {
                host: '192.168.99.100',
                port: 6379,
                lazyConnect: true
            }

            redis = new IORedis(redisOpts);
            await redisMomClient.open();
            resolve(true);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await redisMomClient.close();
            redis.disconnect();
            resolve(true);
        })
    });


    it('| post --> should receive the posted string message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let message = chance.string({ length: 10 });

        // 2. execute test
        await redisMomClient.post(urn, message, options);

        // 3. compre test data
        let result = await redis.lpop(urn);
        result = JSON.parse(result);
        
        assert.strictEqual(result, message);
        

    });

    it('| post --> should receive the posted integer message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let message = chance.integer();

        // 2. execute test
        await redisMomClient.post(urn, message, options);

        // 3. compre test data
        let result = await redis.lpop(urn);
        result = JSON.parse(result);

        assert.strictEqual(result, message);


    });


    it('| post --> should receive the posted float message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let message = chance.floating();

        // 2. execute test
        await redisMomClient.post(urn, message, options);

        // 3. compre test data
        let result = await redis.lpop(urn);
        result = JSON.parse(result);

        assert.strictEqual(result, message);


    });


    it('| post --> should receive the posted date message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let message = chance.date();

        // 2. execute test
        await redisMomClient.post(urn, message, options);

        // 3. compre test data
        let result = await redis.lpop(urn);
        result = JSON.parse(result);
        

        assert.strictEqual(result, message.toISOString());


    });


    it('| post --> should receive the posted object message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let message  = {
            id: chance.natural(),
            str: chance.string({ length: 10 }),
            date: chance.date(),
            email: chance.email(),
            phone: chance.phone(),
        };

        // 2. execute test
        await redisMomClient.post(urn, message, options);

        // 3. compre test data
        let result = await redis.lpop(urn);
        result = JSON.parse(result);

        message.date = message.date.toISOString() as any;        

        assert.deepStrictEqual(result, message);


    });


});
