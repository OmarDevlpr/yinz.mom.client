// libs
import * as assert from "assert";
import { Chance } from "chance";
import * as _ from "lodash";
// import * as stringify from "json-stringify-safe";
// yinz depdendencies
import Container from "@yinz/container/ts/Container";
import RedisMomClient from "../../main/ts/RedisMomClient";

// test dependencies
import * as IORedis from "ioredis";


const chance = Chance();
const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        process.cwd() + '/dist/main/ts'
    ],
});


const redisMomClient = container.getBean<RedisMomClient>("redisMomClient");

let redis;

describe('| mom.client <reply> ', function () {

    before(() => {
        return new Promise(async (resolve) => {
            await redisMomClient.open();
            let redisOpts = {
                host: '192.168.99.100',
                port: 6379,
                lazyConnect: true
            }
            redis = new IORedis(redisOpts);
            resolve(true);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await redisMomClient.close();
            redis.disconnect()
            resolve(true);
        })
    });


    it('| reply --> should reply with a  string message successfully', async () => {

        // 1. prepare test data
        
        let options = {};

        let urn = chance.string({ length: 10 });
        let cid = chance.natural({ min: 1000000000000000, max: 9999999999999999 });
        let rsp = chance.string({ length: 10 });

        // execute test
        await redisMomClient.reply(urn + ':' + cid, rsp, options);        

        // 3. compare result

        let result = await redis.lpop(urn + ':' + cid);
        
        assert.strictEqual( JSON.parse(result), rsp);

        
    });

    
    it('| reply --> should reply with an integer message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let cid = chance.natural({ min: 1000000000000000, max: 9999999999999999 });
        let rsp = chance.integer();

        // execute test
        await redisMomClient.reply(urn + ':' + cid, rsp, options);

        // 3. compare result

        let result = await redis.lpop(urn + ':' + cid);

        assert.strictEqual(JSON.parse(result), rsp);


    });


    it('| reply --> should reply with a float message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let cid = chance.natural({ min: 1000000000000000, max: 9999999999999999 });
        let rsp = chance.floating();

        // execute test
        await redisMomClient.reply(urn + ':' + cid, rsp, options);

        // 3. compare result

        let result = await redis.lpop(urn + ':' + cid);

        assert.strictEqual(JSON.parse(result), rsp);


    });


    it('| reply --> should reply with a date message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let cid = chance.natural({ min: 1000000000000000, max: 9999999999999999 });
        let rsp = chance.date();

        // execute test
        await redisMomClient.reply(urn + ':' + cid, rsp, options);

        // 3. compare result

        let result = await redis.lpop(urn + ':' + cid);

        assert.strictEqual(JSON.parse(result), rsp.toISOString());


    });


    it('| reply --> should reply with an object message successfully', async () => {

        // 1. prepare test data

        let options = {};

        let urn = chance.string({ length: 10 });
        let cid = chance.natural({ min: 1000000000000000, max: 9999999999999999 });
        let rsp = {
            id: chance.natural(),
            str: chance.string({ length: 10 }),
            date: chance.date(),
            email: chance.email(),
            phone: chance.phone(),
        };

        // execute test
        await redisMomClient.reply(urn + ':' + cid, rsp, options);

        // 3. compare result

        let result = await redis.lpop(urn + ':' + cid);

        rsp.date = rsp.date.toISOString() as any;
        assert.deepStrictEqual(JSON.parse(result), rsp);


    });

});
