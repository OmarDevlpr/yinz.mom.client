import {YinzOptions} from "@yinz/commons"

export interface MomMessage {

}

export default interface MsgFilter {
    inward(urn: string, count: number, msg: MomMessage, options?: YinzOptions): Promise<void>;
    outward(urn: string, count: number, msg: MomMessage, options?: YinzOptions): Promise<void>;
}