import MomClient, { MomReq } from "./MomClient";
import MsgFilter, { MomMessage } from "./MsgFilter";
import { Logger, YinzOptions, Exception } from "@yinz/commons";
import * as IORedis  from "ioredis";
import {Redis}  from "ioredis";
import * as _ from "lodash";
import * as util from "util"
import * as Random from "random-js";
import { EventEmitter } from "events";


export interface RedisMomListener {
    redis: YinzRedis;
    id: string;
    canceled: boolean;
}

export interface MomPacket {
    urn: string;
    msg: any;
}

export interface YinzRedis extends Redis {    
    id?: string;
} ;

export interface RedisOptions {
    host: string;
    port: number;
    lazyConnect?: boolean
}

export interface RedisMomClientOptions {    
    appName?: string;
    redis: RedisOptions;
    logger: Logger;
    timeout?: number;
}

export interface MomSubmitter {
    id: string;
    listenerInstalled?: boolean;
}

export default class RedisMomClient implements MomClient {
    
    private _filters: MsgFilter[];
    private _appName: string;
    private _logger: Logger;
    private _redisOptions: RedisOptions;
    private _waitTimeout: number;
    private _redis: YinzRedis;    
    private _closing: boolean;
    private _submitter: MomSubmitter;
    private _eventEmitter: EventEmitter;

    private _listeners: Map<string, RedisMomListener>;
    private _listenersCounter: number;

    private _random: Random;

    private _inwarCounter: number;
    private _outwarCounter: number;

    constructor(options: RedisMomClientOptions) {        
        
        this._logger = options.logger;
        this._appName = options.appName || "momcli";
        this._redisOptions = options.redis;
        this._filters = [];
        this._eventEmitter = new EventEmitter();
        this.init(options);
    }

    private init(options: RedisMomClientOptions & any) {
                        
        this._waitTimeout = options.timeout || 60
        this._closing = false;
        this._listenersCounter = 1;
        this._listeners = new Map();
                        
        this._redisOptions.lazyConnect = true
        this._redis = this.createRedisClient(this._appName, this._redisOptions);

        // generator of ids for listeners
        this._random = new Random();

        this._inwarCounter = 1
        this._outwarCounter = 1
    }

    private createRedisClient(id: string, options: RedisOptions): YinzRedis {
                
        this._logger.info('creating redis client [%s]...', id);        
        let redis = new IORedis(options);

        this._logger.info('installing event handlers on redis client [%s]...', id);
        redis.on('connect', () => {
            this._logger.info('--connect--> connection of redis client [%s] has been established.', id);
        });
        redis.on('ready', () => {
            this._logger.info('--ready--> connection of redis client [%s] is ready.', id);
        });
        redis.on('error', (e) => {
            this._logger.info('--error--> connection of redis client [%s] has errors!', id, e );
        });
        redis.on('close', () => {
            this._logger.info('--close--> connection of redis client [%s] has been closed and reopening it will be retried.', id);
        });
        redis.on('reconnecting', (ms) => {
            this._logger.info('--reconnecting--> connection of redis client [%s] will be reopened in %dms.', id, ms);
        });
        redis.on('end', () => {
            this._logger.info('--end--> connection of redis client [%s] has been closed definitely!', id);
        });
        
        let yinzRedis: YinzRedis = redis;
        yinzRedis.id = id;

        return yinzRedis;
    }

    public async close(options?: YinzOptions): Promise<void> {
        this._closing = true
        
        // // close all listeners
        let lids = Array.from( this._listeners.keys()) ;

        // cancel all non-canceled listeners before closing redis.        
        for ( let lid of lids ) {
            let listener = this._listeners.get(lid);
            if ( listener && listener.canceled === false)            
                await this.cancelListener(lid, false, options)
        }

        await this.releaseClient(this._redis, options);
    }

    public async open(options?: YinzOptions & any): Promise<void> {
        options = options || {};
        this.init(options);
        await this.connectRedisClient(this._redis, options);
    }

    private async connectRedisClient(redis: YinzRedis, options?: YinzOptions & any): Promise<void> {
                
        if (redis.status === 'active' || redis.status === 'connecting') {
            return; 
        }

        this._logger.info('connecting client [%s] to redis...', redis.id);

        await redis.connect();

        this._logger.info('...client [%s] has been connected to redis.', redis.id);

    }

    public async cancelListener(lid: string, failOnNotFound: boolean, options?: YinzOptions ): Promise<boolean> {
        
        this._logger.debug('cancelling listener [%s]...', lid);

        let listener =   this._listeners.get(lid);

        if ( ! listener ) {

            if (failOnNotFound) {
                throw new Exception('ER_MOM_CLI_REDIS____CANCEL_LSTNR____LSTNR_NOT_FOUND', {
                    message: 'The supplied listener has not been installed before.'
                });
            }
            this._logger.warn('listener [%s] has not been cancelled s it does not exist!', lid);
            return false;
        }

        listener.canceled = true;

        await this.releaseClient(listener.redis, options);

        this._logger.debug('...listener [%s] has been marked as cancelled.', lid);

        return true;

    }

    private async releaseClient(redis: YinzRedis, options?: YinzOptions): Promise<void> {
        this._logger.info('closing redis client [%s]...', redis.id);
        // await redis.disconnect()
        await redis.quit()
        this._logger.info('...redis client [%s] has been closed.', redis.id);                
    }

    subscribe(filter: MsgFilter): void {
        this._filters.push(filter)        
    }


    async submit(urn: string, req: MomReq, options?: YinzOptions & {timeout?: number}): Promise<any> {
                
        const submitOptions = Object.assign({}, options, { logger: this._logger },  );
        
        
        const cid           = this._appName + ':' + urn + ':' + this._random.hex(16);
        const timeout       = options && options.timeout || 30;                                        

        this._submitter = this._submitter || { id: this._appName + ":submitter#" + this._random.hex(8) };

        const msg = { 
            cid     : cid,
            data    : req,
            replyTo : this._submitter.id
        };   

        // submitter id
        const sid = this._submitter.id;

        if ( !this._submitter.listenerInstalled ) {
            
            this.listen([sid], (urn: string, msg: any, options) => {                
                this._eventEmitter.emit(msg.cid, msg.data, options);
            }, sid, submitOptions);

            this._submitter.listenerInstalled = true;
        }            
                        
        return new Promise((resolve, reject) => {         
                        
            this._eventEmitter.once(cid, (data, options) => {                                                    
                resolve(data)
            })

            _.delay(() => {

                // remove the listener that wait for the response
                this._eventEmitter.removeAllListeners(cid);
                
                return reject( new Exception("ER_MOM_CLI_REDIS___SUBMIT___TIMEOUT", {
                    error: '',
                    message: `No message received on [%${cid}] within the last [%${timeout}].`
                }))

            }, timeout * 1000);            
        
            // push request to queue
            this.push(this._redis, urn, msg, submitOptions);          
        })
                
    }


    async reply(replyTo: string, rsp: any, options?: YinzOptions): Promise<void> {
        return await this.push(this._redis, replyTo, rsp, options);
    }
    async post(urn: string, msg: MomMessage, options?: YinzOptions): Promise<void> {
        return await this.push(this._redis, urn, msg, options);
    }

    private async push(redis: YinzRedis, urn: string, msg: any, options?: YinzOptions): Promise<void> {

        this._logger.debug('pushing a message on [%s] using client [%s]...', urn, redis.id);

        await this.filterMessage("outward", urn, msg, options);

        let value = JSON.stringify(msg);
        
        
        await redis.rpush(urn, value);
                             
    }

    private async pop (redis: YinzRedis, urns: string[], waitTimeout: number, options?: YinzOptions ): Promise<any> {
                            
        if (this._closing) {
            throw new Exception('ER_MOM_CLI_REDIS____POP____SHUTTING_DOWN', {
                message: util.format('The client is shutting down => will stop listening for messages on [%s]', urns),
            });
        }

        this._logger.debug('will wait a message on [%s] for [%ds]...', urns, waitTimeout);


        try {

            let res =  await redis.blpop(urns as any, waitTimeout as any)
            
            if ( res === null ) {
                this._logger.debug('...timeout on [%s]', urns);
                throw new Exception('ER_MOM_CLI_REDIS____POP____TIMEOUT', {
                    message: util.format('No message received on [%s] within the last [%ds].', urns, waitTimeout),
                });
            }            

            let packet: MomPacket = {
                urn: res[0],
                msg: JSON.parse(res[1])
            }

            await this.filterMessage("inward", packet.urn, packet.msg, options)

            this._logger.debug('...has received on [%s] message [%s].', packet.urn, JSON.stringify(packet.msg));
            return packet;

        } catch (e) {

            this._logger.debug('...error on [%s]: %s', urns, e.message)
            throw e
        }
                                                                            
    }

    async listen(urns: string[], handler: any, lid: string, options?: YinzOptions): Promise<string | any> {
                
        lid = lid || this._appName + ':listener#' + this._listenersCounter++;

        this._logger.debug('installing listener on urns %j...', urns);        

        let listener = this._listeners.get(lid) || {canceled: false, id: lid } as RedisMomListener;

        if ( listener && listener.canceled ) {
            this._logger.debug('...will not install listener on urns %j as it has been marked cancelled!', urns);            
            return;  
        }

        this._logger.debug('...listener on urns %j has been installed.', urns);
            
        if ( !listener.redis ) {
            listener.redis = this.createRedisClient(lid, this._redisOptions)                        
        }            

        this._listeners.set(lid, listener);
                
        Promise.resolve().then( async () => {

            try {
                let packet: MomPacket = await this.pop(listener.redis, urns, this._waitTimeout, options);

                this._logger.debug('forewarding received message to handler [%s]...', packet.urn);
                
                await handler(packet.urn, packet.msg, options);
            } catch (e) {
                throw e;
            }
            
        }).catch( async e => {
            
            if (e instanceof Exception) {

                if (_.endsWith(e.reason, '__TIMEOUT')) {
                    // ignore timeouts    
                    return;
                }
            }

            this._logger.warn('Error within listener: %s', (e.stack || e.message));
            // throw e;                
            
        })
        .then( async () => {
            this._logger.debug(`will listen again:
            - urns: [${urns}]
            - handler: FUNCTION
            - lid: ${lid}
            - options : ${options}            
            `)
            
            await this.listen(urns, handler, lid, options);     
        })
                                
        return lid;                       
    }




    private async filterMessage(dir: "inward" | "outward", urn: string, msg: any, options?: YinzOptions): Promise<any> {
        
        if (dir === 'inward') {
            let seq = this._inwarCounter++;
            if (_.isPlainObject(msg)) {
                msg.seq = seq;
            }

            for ( let filter of this._filters ) {
                await filter.inward(urn, seq, msg, options)
            }
            
        }
        else {
            let seq = _.isPlainObject(msg) && msg.seq || this._outwarCounter++;

            for (let filter of this._filters) {
                await filter.outward(urn, seq, msg, options)
            }            
        }
            
    }

    
}
