import MsgFilter, { MomMessage } from "./MsgFilter";
import { YinzOptions } from "@yinz/commons";

export interface MomReq {

}

export interface MomRsp {
    data: any;
}

export default interface MomClient {

    subscribe(filter: MsgFilter): void;

    submit(urn: string, req: MomReq, options?: YinzOptions ): Promise<any>;

    reply(replyTo: string, rsp: any, options?: YinzOptions ): Promise<void>;

    post(urn: string, msg: MomMessage, options?: YinzOptions): Promise<void>;

    listen(urns: string[], handler, lid: string, options?: YinzOptions): Promise<any | string>;

    open(options?: YinzOptions & any): Promise<void>;

    cancelListener(lid: string, failOnNotFound: boolean, options?: YinzOptions): Promise<boolean>;

    close(options?: YinzOptions): Promise<void>;
}