import MsgFilter, { MomMessage } from "./MsgFilter";
import { Logger, YinzOptions } from "@yinz/commons";
import * as stringify from "json-stringify-safe";
export interface MsgLoggerMsgFilterOptions {
    logger: Logger;
}

const sep = '--------------------------------------------------------------------------------';

export default class MsgLoggerMsgFilter implements MsgFilter {

    private _logger: Logger;

    constructor(options: MsgLoggerMsgFilterOptions) {
        this._logger = options.logger;
    }

    public async inward(urn: string, count: number, msg: MomMessage, options?: YinzOptions): Promise<void> {
        this.print('in', urn, count, msg, options);
    }
    public async outward(urn: string, count: number, msg: MomMessage, options?: YinzOptions): Promise<void> {
        this.print('out', urn, count, msg, options);
    }

    private async print(dir: "in" | "out" , urn: string, count: number, msg: any, options?: YinzOptions): Promise<void> {

        let label = (dir === 'in' ? 'I n b o u n d   M e s s a g e' : 'O u t b o u n d   M e s s a g e');

        this._logger.info(
            '\n%s\nseq: %d\nto:  %s\nmsg: %s\n%s',
            ('- ' + label + ' ' + sep).substring(0, sep.length),
            count,
            urn,
            stringify(msg, null, 2),
            sep
        ); 

    }

}