
import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import MsgLoggerMsgFilter from "./MsgLoggerMsgFilter";
import RedisMomClient from "./RedisMomClient";


const registerBeans = (container: Container) => {

    let logger = container.getBean<Logger>('logger');

    let msgLoggerMsgFilter = new MsgLoggerMsgFilter({
        logger
    });

    let redisMomClient = new RedisMomClient({
        logger,
        appName: container.getParam("srvmng") && container.getParam("srvmng")["appName"] as string,
        redis: container.getParam("redis")
        // timeout: container.getParam("redis") && container.getParam("redis").timeout
    })
    
    container.setBean("msgLoggerMsgFilter", msgLoggerMsgFilter)
    
    container.setBean("redisMomClient", redisMomClient)
    
};

const registerClazzes = (container: Container) => {

    container.setClazz("RedisMomClient", RedisMomClient)

};


export {
    registerBeans,
    registerClazzes
};